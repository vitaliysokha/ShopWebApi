﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.UnitOfWork;
using Microsoft.AspNetCore.Cors;
using ShopWebApi.DTO;

namespace ShopWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Products")]
    [EnableCors("MyPolicy")]
    public class ProductsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;


        public ProductsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<IEnumerable<Product>> GetProducts()
        {
           return  await _unitOfWork.Product.GetWithInclude(x=>x.Category);
        }

        [HttpGet("GetProductsForCategory")]
        public IActionResult GetProductsForCategory(int categoryId)
        {
            var result = _unitOfWork.Product.Get(x => x.CategoryId == categoryId);
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _unitOfWork.Product.FindByIdAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct( [FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            await _unitOfWork.Product.Update(product);
            await _unitOfWork.Save();
            return Ok();
        }

        // POST: api/Products
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] ProductDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var product = Mapper.Map<ProductDTO, Product>(model);

            await _unitOfWork.Product.CreateAsync(product);
            await _unitOfWork.Save();
            return Ok(product);

        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            await _unitOfWork.Product.Remove(id);
            await _unitOfWork.Save();
            return Ok();
        }
       
        [HttpPost("AddOrder")]
        public async Task<IActionResult> AddOrder(int productId)
        {
            var order = new Order
            {
                UserId = "68f61952-0eee-487f-beee-f15ae2c7a281",
                Date = DateTime.Now
            };
           
           await _unitOfWork.Order.CreateAsync(order);

            return Ok(order);

        }

        [HttpPost("AddProductToOrder")]
        public async Task<IActionResult> AddProductToOrder(int productId)
        {
            var product = new OrderedProduct
            {
                OrderId = 1,
                Quentity = 1,
                ProductId = productId,
                
            };
            await _unitOfWork.OrderedProduct.CreateAsync(product);

            return Ok(product);

        }

        [HttpGet("GetOrderedProducts")]
        public IActionResult GetOrderedProducts(int orderId = 1)
        {
            var result = _unitOfWork.OrderedProduct.DbSet.Where(x=>x.OrderId==orderId)
                .Select(x => new
                {
                    ProductName = x.Product.Name,
                    Count = x.Quentity,
                    Price = x.Product.Price
                });
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }



    }
}