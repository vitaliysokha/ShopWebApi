﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Services.AccountService;
using Services.LoginService;
using ShopWebApi.DTO;

namespace ShopWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    [EnableCors("MyPolicy")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly ILoginService _loginService;


        public AccountController(
            IAccountService accountService,
            ILoginService loginService)
        {
            _accountService = accountService;
            _loginService = loginService;
        }


        [AllowAnonymous]
        [HttpPost("token")]
        public async Task<IActionResult> Token([FromBody] LoginRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var identity = await _loginService.GetIdentity(request.Login, request.Password);
            if (identity.Error != null)
            {
                return BadRequest(identity.Error);
            }

            var response = _loginService.GetToken(identity.Data);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterRequest model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await _accountService.Register(model);
            if (result.Error != null)
            {
                return StatusCode(result.Error.ErrorCode, result.Error.ErrorDescription);
            }

            return Ok(result.Data);
        }
    }
}