﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using DataAccessLayer.UnitOfWork;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopWebApi.DTO;

namespace ShopWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Categories")]
    [EnableCors("MyPolicy")]
    public class CategoriesController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoriesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: api/Categories
        [HttpGet]
        public async Task<IEnumerable<CategoryDTO>> GetCategories()
        {
            var result = await _unitOfWork.Category.GetWithInclude(x => x.Name.Contains("d"), x => x.Name, x => x.Products);
            var categories = Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDTO>>(result);
            return categories;
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var category = await _unitOfWork.Category.FindByIdAsync(id);

            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }



        // POST: api/Categories
        [HttpPost]
        public async Task<IActionResult> PostCategory([FromBody] CategoryDTO model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var category = Mapper.Map<CategoryDTO, Category>(model);
            await _unitOfWork.Category.CreateAsync(category);
            await _unitOfWork.Save();
            return Ok(model);
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategory([FromRoute] int id)
        {

            await _unitOfWork.Category.Remove(id);
            await _unitOfWork.Save();

            return Ok();
        }

    }
}