﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DataAccessLayer.Entities;
using DataAccessLayer.UnitOfWork;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShopWebApi.DTO;

namespace ShopWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Baskets")]
    [EnableCors("MyPolicy")]
    public class BasketsController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public BasketsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        public async Task<IActionResult> PostBasket(string userId, int productId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
          
            await _unitOfWork.Basket.AddToBasket(userId, productId);
            var product = await  _unitOfWork.Product.FindByIdAsync(productId);
            var basket = await _unitOfWork.Basket.FindByIdAsync(userId);
            basket.Amount += (double)product.Price;
            await _unitOfWork.Basket.Update(basket);
            await _unitOfWork.Save();
            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBasket([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var basket = await _unitOfWork.Basket.GetBasket(id);
            
            

            return Ok(basket);
        }
    }
}