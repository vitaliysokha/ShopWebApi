﻿using System.Collections.Generic;
using DataAccessLayer.Entities;

namespace ShopWebApi.DTO
{
    public class BasketResponse
    {
        public double Amount { get; set; }
        public ICollection<OrderedProduct> OrderedProducts { get; set; }
    }
}