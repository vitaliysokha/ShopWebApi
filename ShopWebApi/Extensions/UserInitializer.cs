﻿using System;
using System.Security.Claims;
using Common.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;


namespace ShopWebApi.Extensions
{
    public class UserInitializer
    {
        public static void Initialize(IServiceProvider services)
        {
            using (var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                //   var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();


                if (roleManager.FindByNameAsync(Roles.Admin.ToString()).Result == null)
                {
                    var role = new IdentityRole();
                    role.Name = Roles.Admin.ToString();
                    roleManager.CreateAsync(role).Wait();
                    roleManager.AddClaimAsync(role,
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, Roles.Admin.ToString())).Wait();
                }
                if (roleManager.FindByNameAsync(Roles.User.ToString()).Result == null)
                {
                    var role = new IdentityRole();
                    role.Name = Roles.User.ToString();
                    roleManager.CreateAsync(role).Wait();
                    roleManager.AddClaimAsync(role,
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, Roles.User.ToString())).Wait();
                }
            }
        }
    }
}