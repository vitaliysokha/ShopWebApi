﻿using System.Threading.Tasks;
using Common.DTO.Communication;
using Common.Enums;
using ShopWebApi.DTO;

namespace Services.AccountService
{
    public interface IAccountService
    {
        Task<Response<OperationResult>> Register(RegisterRequest model);
    }
}