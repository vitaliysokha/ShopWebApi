﻿using System;
using System.Threading.Tasks;
using Common.DTO.Communication;
using Common.Enums;
using DataAccessLayer.Entities;
using DataAccessLayer.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ShopWebApi.DTO;

namespace Services.AccountService
{
    public class AccountService: IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUnitOfWork _unitOfWork;

        public AccountService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager,
            IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWork = unitOfWork;
        }

        public async Task<Response<OperationResult>> Register(RegisterRequest model)
        {
            var response = new Response<OperationResult>()
            {
                Data = OperationResult.Failed
            };

            var user = new User
            {
                Email = model.Email,
                UserName = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
            };
            var userExixt = await _unitOfWork.User.DbSet.FirstOrDefaultAsync(x => x.Email == model.Email);
            if (userExixt != null)
            {
                response.Error = new Error(404, "This user is already registered!");
                return response;
            }

            var creationUser = await _userManager.CreateAsync(user, model.Password);

            if (creationUser.Succeeded)
            {
                var userBasket = new Basket
                {
                    Id = user.Id,
                    Amount = 0,
                    UserId = user.Id
                };
                await _unitOfWork.Basket.CreateAsync(userBasket);
                await _unitOfWork.Save();


                var addingToRole = await _userManager.AddToRoleAsync(user, Roles.User.ToString());
                if (addingToRole.Succeeded)
                {
                    response.Data = OperationResult.Success;
                    return response;
                }

                response.Error = new Error(404, "Error while adding role to user!");
                return response;
            }

            foreach (var error in creationUser.Errors)
            {
                response.Error = new Error(404, error.Description);
            }

            return response;
        }
    }
}