﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Common.DTO.Communication;
using Common.DTO.Login;
using Common.Helpers;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using DataAccessLayer.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using ShopWebApi.Utils;


namespace Services.LoginService
{
    public class LoginService : ILoginService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
   

        public LoginService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager,
            IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }


        public TokenResponse GetToken(ClaimsIdentity identity)
        {
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: DateTime.UtcNow,
                claims: identity.Claims,
                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var tokenResponse = new TokenResponse
            {
                Token = encodedJwt,
                Roles = identity.Claims?.Where(p => p.Type == ClaimsIdentity.DefaultRoleClaimType).Select(x => x.Value)
                    .ToList(),
                UserId = identity.Name
            };

            return tokenResponse;
        }

        public async Task<Response<ClaimsIdentity>> GetIdentity(string login, string password)
        {
            Response<ClaimsIdentity> response = new Response<ClaimsIdentity>();

            var user = await GetUserByCreds(login, password);
            if (user == null)
            {
                response.Error = new Error(404, "Invalid username or password");
                return response;
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            List<Claim> claims = new List<Claim>();
            foreach (var role in userRoles)
            {
                var identityRole = await _roleManager.FindByNameAsync(role);
                var cl = await _roleManager.GetClaimsAsync(identityRole);
                claims.AddRange(cl);
            }
            var claimUserId = new Claim(ClaimsIdentity.DefaultNameClaimType, user.Id);

            claims.Add(claimUserId);

            ClaimsIdentity claimsIdentity = new ClaimsIdentity
                (claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            response.Data = claimsIdentity;
            return response;
        }

        public async Task<User> GetUserByCreds(string login, string password)
        {
            
        //    var encrypted = TripleDESCryptHelper.Encript(password);
            var user = await _userManager.Users.FirstOrDefaultAsync(
                p => p.Email == login);
            var verify = await _userManager.CheckPasswordAsync(user, password);
            if (!verify)
            {
                return null;
            }
            return user;
        }
    }
}