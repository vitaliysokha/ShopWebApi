﻿using System.Security.Claims;
using System.Threading.Tasks;
using Common.DTO.Communication;
using Common.DTO.Login;
using DataAccessLayer.Entities;

namespace Services.LoginService
{
    public interface ILoginService
    {
        Task<User> GetUserByCreds(string login, string password);

        Task<Response<ClaimsIdentity>> GetIdentity(string login, string password);

        TokenResponse GetToken(ClaimsIdentity identity);
    }
}