﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace ShopWebApi.Utils
{
    public class AuthOptions
    {
        public const string ISSUER = "magnis_space_web_api_server";
        public const string AUDIENCE = "magnis_space_web_api_users";
        const string KEY = "security_80_lvl.ty_ne_proydesh";
        public const int LIFETIME = 60 * 24;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}