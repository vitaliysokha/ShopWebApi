﻿using System.Collections.Generic;

namespace Common.DTO.Login
{
    public class TokenResponse
    {
        public string Token { get; set; }
        public List<string> Roles { get; set; }
        public string UserId { get; set; }
    }
}