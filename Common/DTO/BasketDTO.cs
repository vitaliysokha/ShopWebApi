﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTO
{
    public class BasketDTO
    {
        public double Amount { get; set; }
        public IEnumerable<ProductsDTO> Products { get; set; }
    }
}
