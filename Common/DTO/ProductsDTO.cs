﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTO
{
    public class ProductsDTO
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quentity { get; set; }
    }
}
