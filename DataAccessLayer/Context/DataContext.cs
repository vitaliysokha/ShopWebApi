﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Context
{
   public class DataContext: IdentityDbContext<User>
    {

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<OrderedProduct> OrderedProducts { get; set; }
        public virtual DbSet<Basket> Baskets { get; set; }
        public virtual DbSet<Entities.Attribute> Attributes { get; set; }
        public virtual DbSet<AttributeDescription> AttributeDescriptions { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<User>().HasOne(u => u.Profile).WithOne(x => x.User)
                .HasForeignKey<UserProfile>(p => p.UserId);
            builder.Entity<OrderedProduct>().HasKey(x => new { x.OrderId, x.ProductId });
            builder.Entity<IdentityUser>().ToTable("Users");
            builder.Entity<IdentityUser>().Ignore(x => x.AccessFailedCount);
            builder.Entity<IdentityUser>().Ignore(x => x.LockoutEnabled);
            builder.Entity<IdentityUser>().Ignore(x => x.ConcurrencyStamp);
            builder.Entity<IdentityUser>().Ignore(x => x.EmailConfirmed);
            builder.Entity<IdentityUser>().Ignore(x => x.LockoutEnd);
            builder.Entity<IdentityUser>().Ignore(x => x.PhoneNumberConfirmed);
            builder.Entity<IdentityUser>().Ignore(x => x.TwoFactorEnabled);
            

            builder.Entity<AttributeDescription>().HasKey(x => new {x.AttributeId, x.ProductId});
            
        }

      
    }
}
