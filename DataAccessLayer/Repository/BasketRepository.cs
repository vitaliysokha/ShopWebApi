﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Common.DTO;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Repository
{
    public class BasketRepository : Repository<Basket>, IBasketRepository
    {
        public BasketRepository(DataContext context) : base(context)
        {
        }

        public async Task AddToBasket(string userId, int productId)
        {
            await _dataContext.OrderedProducts.AddAsync(new OrderedProduct
            {
                BasketId = userId,
                ProductId = productId,
                Quentity = 1,
                OrderId = 2
            });
        }

        public async Task<BasketDTO> GetBasket(string userId)
        {
            var basket = await _dataContext.Baskets.Include(x => x.OrderedProducts)
                .Where(x=>x.Id == userId)
                .Select(x=>new BasketDTO
                {
                    Amount = x.Amount,
                    Products = x.OrderedProducts.Select(p=> new ProductsDTO
                    {
                        Name = p.Product.Name,
                        Price = p.Product.Price,
                        Quentity = p.Quentity
                    } )
                }).FirstOrDefaultAsync();
            return basket;
        }
    }

   

    
}