﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repository
{
   public class OrderedProductRepository: Repository<OrderedProduct>, IOrderedProductRepository
    { 
        public OrderedProductRepository(DataContext context) : base(context)
        {

        }


    }
}
