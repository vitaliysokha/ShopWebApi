﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DataAccessLayer.Context;
using DataAccessLayer.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Remotion.Linq.Utilities;

namespace DataAccessLayer.Repository
{
    public class Repository<TEntity>: IRepository<TEntity> where TEntity : class
    {
        protected DataContext _dataContext;
        protected DbSet<TEntity> _dbSet;
        
        public Repository(DataContext context)
        {
            _dataContext = context;
            _dbSet = context.Set<TEntity>();
        }

        public DbSet<TEntity> DbSet
        {
            get {
                if (_dbSet == null)
                {
                    _dbSet = _dataContext.Set<TEntity>();
                }
                return _dbSet;
            }
        }

        public async Task CreateAsync(TEntity item)
        {
            await _dbSet.AddAsync(item);
          //  await _dataContext.SaveChangesAsync();
        }

        public async Task<TEntity> FindByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }
        public async Task<TEntity> FindByIdAsync(string id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await _dbSet.AsNoTracking().ToListAsync();
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return  _dbSet.AsNoTracking().Where(predicate).ToList();
        }

        

        public async Task<IEnumerable<TEntity>> GetWithInclude(params Expression<Func<TEntity, object>> [] includeProperties)
        {
            var include = Include(includeProperties) ;   

            return await include.ToListAsync();
        }

      
        private IQueryable<TEntity> Include(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _dbSet.AsNoTracking();
            return includeProperties
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        public async Task Remove(int id)
        {
            var item = await  _dbSet.FindAsync(id);
            if (item == null)
            {
                throw new Exception("Item not found");
            }
           _dbSet.Remove(item);
           await _dataContext.SaveChangesAsync();
        }

        public async Task Update(TEntity item)
        {
            _dataContext.Entry(item).State = EntityState.Modified;
            await _dataContext.SaveChangesAsync();
        }

      
       
    }
}