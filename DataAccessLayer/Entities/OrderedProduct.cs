﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace DataAccessLayer.Entities
{
   public class OrderedProduct
    {
        public string BasketId { get; set; }

        public int? ProductId { get; set; }

        public int? OrderId { get; set; }

        public int Quentity { get; set; }

        public Order Order { get; set; }

        [JsonIgnore]
        public Basket Basket { get; set; }

        public Product Product { get; set; }


    }
}
