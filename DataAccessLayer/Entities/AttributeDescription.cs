﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class AttributeDescription
    {
        public int? ProductId { get; set; }

        public int? AttributeId { get; set; }

        public Product Product { get; set; }

        public Attribute Attribute { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

    }
}
