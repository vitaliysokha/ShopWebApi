﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayer.Entities
{
   public class Basket
    {
        public string Id { get; set; }

        public double Amount { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public ICollection<OrderedProduct> OrderedProducts { get; set; }

        public Basket()
        {
            OrderedProducts = new List<OrderedProduct>();
        }


    }
}
