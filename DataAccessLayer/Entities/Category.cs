﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Newtonsoft.Json;

namespace DataAccessLayer.Entities
{
   public class Category
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [JsonIgnore]
        public ICollection<Product> Products { get; set; }

        public ICollection<Attribute> Attributes { get; set; }

        public Category()
        {
           // Products = new List<Product>();
           // Attributes = new List<Attribute>();
        }

    }
}
