﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Newtonsoft.Json;

namespace DataAccessLayer.Entities
{
   public class Product
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }

        public int? CategoryId { get; set; }

       
        public Category Category { get; set; }

        [JsonIgnore]
        public ICollection<OrderedProduct> OrderedProducts { get; set; }

        public Product()
        {
      //      OrderedProducts = new List<OrderedProduct>();
        }
    }
}
