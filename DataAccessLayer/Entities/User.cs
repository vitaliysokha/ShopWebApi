﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;

namespace DataAccessLayer.Entities
{
    public class User: IdentityUser
    {

        public UserProfile Profile { get; set; }

        public ICollection<Order> Orders { get; set; }

        public Basket Basket { get; set; }

        
        public User()
        {
            Orders = new List<Order>();
        }
    }
}
