﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccessLayer.Entities
{
   public class Order
    {
     
        public int Id { get; set; }

        public string UserId { get; set; }

        public string Status { get; set; }

        public double Amount { get; set; }

        public DateTime Date { get; set; }

        public User Customer { get; set; }

        public ICollection<OrderedProduct> OrderedProducts { get; set; }

        public Order()
        {
            OrderedProducts = new List<OrderedProduct>();
        }
    }
}
