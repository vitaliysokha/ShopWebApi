﻿using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface IProductRepository: IRepository<Product>
    {
        
    }
}