﻿using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface ICategoryRepository:IRepository<Category>

    {

    }
}