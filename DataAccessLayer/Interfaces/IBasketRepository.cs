﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.DTO;
using DataAccessLayer.Entities;
using DataAccessLayer.Repository;

namespace DataAccessLayer.Interfaces
{
    public interface IBasketRepository: IRepository<Basket>
    {
        Task AddToBasket(string userId, int productId);
        Task<BasketDTO> GetBasket(string userId);
    }
}