﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class 
    {
        DbSet<TEntity> DbSet { get; }
        Task CreateAsync(TEntity item);
        Task<TEntity> FindByIdAsync(int id);
        Task<TEntity> FindByIdAsync(string id);
        Task<IEnumerable<TEntity>> GetAsync();
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);

        Task<IEnumerable<TEntity>> GetWithInclude(params Expression<Func<TEntity, object>>[] includeProperties);

        Task Remove(int id);
        Task Update(TEntity item);
    }
}