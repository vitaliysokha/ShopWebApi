﻿using DataAccessLayer.Entities;

namespace DataAccessLayer.Interfaces
{
    public interface IOrderRepository:IRepository<Order>
    {
        
    }
}