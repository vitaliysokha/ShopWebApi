﻿using System.Threading.Tasks;
using DataAccessLayer.Context;
using DataAccessLayer.Interfaces;
using DataAccessLayer.Repository;

namespace DataAccessLayer.UnitOfWork
{
    public class UnitOfWork :IUnitOfWork
    {
        private IOrderedProductRepository _orderedProduct;
        private ICategoryRepository _category;
        private IUserRepository _user;
        private IProductRepository _product;
        private IOrderRepository _order;
        private IBasketRepository _basket;

        private readonly DataContext _dataContext;

        public UnitOfWork(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IOrderedProductRepository OrderedProduct
        {
            get { return _orderedProduct = _orderedProduct ?? new OrderedProductRepository(_dataContext); } 
        }

        public ICategoryRepository Category
        {
            get { return _category = _category ?? new CategoryRepository(_dataContext); }
        }

        public IUserRepository User
        {
            get { return _user = _user ?? new UserRepository(_dataContext); }
        }

        public IProductRepository Product
        {
            get { return _product = _product ?? new ProductRepository(_dataContext); }
        }

        public IOrderRepository Order
        {
            get { return _order = _order ?? new OrderRepository(_dataContext); }
        }

        public IBasketRepository Basket
        {
            get { return _basket = _basket ?? new BasketRepository(_dataContext); }
        }

        public void Dispose()
        {
            _dataContext.Dispose();
        }

        public async Task Save()
        {
          await  _dataContext.SaveChangesAsync();
        }


      


    }
}