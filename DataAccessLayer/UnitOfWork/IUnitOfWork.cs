﻿using System;
using System.Threading.Tasks;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.UnitOfWork
{
    public interface IUnitOfWork:IDisposable
    {
        IOrderedProductRepository OrderedProduct { get; }
        ICategoryRepository Category { get; }
        IUserRepository User{ get; }
        IProductRepository Product { get; }
        IOrderRepository Order { get; }
        IBasketRepository Basket { get; }
        Task Save();
    }
}